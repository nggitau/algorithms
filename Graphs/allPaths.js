var allPathsSourceTarget = function(graph) {
    const adjList = {}
    for (let i = 0; i < graph.length; i++) {
        adjList[i] = graph[i]
    }

    const paths = []

    const dfs = (current, target, path = [0]) => {
        if (current === target) {
            paths.push(path)
        }

        for (let item of adjList[current]) {
            dfs(item, target, path.concat(item))
        }
    }

    dfs(0, graph.length - 1, [0])
    return paths
};

allPathsSourceTarget([[1,2], [3], [3], []])

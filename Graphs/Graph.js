class Graph {
    constructor() {
        this.adjacencyList = {}
    }

    addVertex(vertex) {
        if (!this.adjacencyList[vertex]) {
            this.adjacencyList[vertex] = []
        }
        return this.adjacencyList[vertex]
    }

    addEdge(vertex1, vertex2) {
        // undirected
        if (this.adjacencyList[vertex1] && this.adjacencyList[vertex2]) {
            this.adjacencyList[vertex1].push(vertex2)
            this.adjacencyList[vertex2].push(vertex1)
            return true
        }
        return
    }

    removeEdge(v1, v2) {
        if (this.adjacencyList[v1] && this.adjacencyList[v2]) {
            this.adjacencyList[v1] = this.adjacencyList[v1].filter(vertex => vertex !== v2)
            this.adjacencyList[v2] = this.adjacencyList[v2].filter(vertex => vertex !== v1)
            return true
        }
        return
    }

    removeVertex(vertex) {
        // go through graph and find anywhere ther vertex is referenced
        // remove vertex from those places
        // delete vertex from adjacencyList
        if (!this.adjacencyList[vertex]) {
            return
        }

        while(this.adjacencyList[vertex].length) {
            const referencedVertex = this.adjacencyList[vertex].pop()
            this.removeEdge(referencedVertex, vertex)
        }

        delete this.adjacencyList[vertex]
        return true
    }

    DFSRecursive(vertex) {
        const results = []
        const visited = {}

        const dfs = (vertex) => {
            if (!vertex) {
                return
            }

            if (vertex && visited[vertex]) {
                return
            }

            visited[vertex] = true
            results.push(vertex)

            for (let item of this.adjacencyList[vertex]) {
                if (!visited[item]) {
                    dfs(item)
                }
            }
        }

        dfs(vertex)
        return results
    }

    DFSIterative(start) {
        const stack = []
        const results = []
        const visited = {}

        stack.push(start)
        visited[stack] = true

        while(stack.length) {
            const current = stack.pop()
            results.push(current)

            for (let v of this.adjacencyList[current]) {
                if (!visited[v]) {
                    visited[v] = true
                    stack.push(v)
                }
            }
        }
        return results
    }

    BFS(start) {
        const results = []
        const visited = {}
        const queue = []

        queue.push(start)
        visited[start] = true

        while(queue.length) {
            const current = queue.shift()
            results.push(current)
            for (let adj of this.adjacencyList[current]) {
                if (!visited[adj]) {
                    queue.push(adj)
                    visited[adj] = true
                }
            }
        }
        return results
    }
}

const graph = new Graph()
graph.addVertex('A')
graph.addVertex('B')
graph.addVertex('C')
graph.addVertex('D')
graph.addVertex('E')
graph.addVertex('F')

graph.addEdge('A', 'B')
graph.addEdge('A', 'C')
graph.addEdge('B', 'D')
graph.addEdge('C', 'E')
graph.addEdge('D', 'E')
graph.addEdge('D', 'F')
graph.addEdge('E', 'F')

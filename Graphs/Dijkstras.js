// const PriorityQueue = require('../Heaps/priorityQueue')
class PriorityQueue {
    constructor() {
        this.values = []
    }

    enqueue(val, priority) {
        this.values.push({ val, priority })
        this.sort()
    }

    dequeue() {
        return this.values.shift()
    }

    sort() {
        this.values.sort((a, b) => a.priority - b.priority)
    }
}


class WeightedGraph {
    constructor() {
        this.adjacencyList = {}
    }

    addVertex(v) {
        if (!this.adjacencyList[v]) {
            this.adjacencyList[v] = []
        }
    }

    addEdge(v1, v2, weight) {
        if (this.adjacencyList[v1] && this.adjacencyList[v2]) {
            this.adjacencyList[v1].push({node: v2, weight})
            this.adjacencyList[v2].push({node: v1, weight})
        }

    }

    Dijkstra(start, end) {
        const nodes = new PriorityQueue()
        const distances = {}
        const prev = {}
        const path = []
        let smallest

        // initial state
        for (let vertex in this.adjacencyList) {
            if (vertex === start) {
                distances[vertex] = 0
                nodes.enqueue(vertex, 0)
            } else {
                distances[vertex] = Infinity
                nodes.enqueue(vertex, Infinity)
            }
            prev[vertex] = null
        }

        // as long as there is something to visit
        while(nodes.values.length) {
            smallest = nodes.dequeue().val
            if (smallest === end) {
                // done, build path to return
                while(prev[smallest]) {
                    path.push(smallest)
                    smallest = prev[smallest]
                }
                break
            }

            if (smallest || distances[smallest] !== Infinity) {
                for (let neighbor of this.adjacencyList[smallest]) {
                    // calculate new distance to neighboring node
                    let newSummedWeight = distances[smallest] + neighbor.weight
                    if (newSummedWeight < distances[neighbor.node]) {
                        distances[neighbor.node] = newSummedWeight
                        prev[neighbor.node] = smallest
                        nodes.enqueue(neighbor.node, newSummedWeight)
                    }
                }
            }
        }

        return path.concat(smallest).reverse()

    }
}

const graph = new WeightedGraph()
graph.addVertex('A')
graph.addVertex('B')
graph.addVertex('C')
graph.addVertex('D')
graph.addVertex('E')
graph.addVertex('F')

graph.addEdge('A', 'B', 4)
graph.addEdge('A', 'C', 2)
graph.addEdge('B', 'E', 3)
graph.addEdge('C', 'D', 2)
graph.addEdge('C', 'F', 4)
graph.addEdge('D', 'E', 3)
graph.addEdge('D', 'F', 1)
graph.addEdge('E', 'F', 1)

graph.Dijkstra('A', 'E')

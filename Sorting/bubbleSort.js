function swap(arr, idx1, idx2) {
	const temp = arr[idx1];
	arr[idx1] = arr[idx2];
	arr[idx2] = temp;
}

function bubbleSort(arr) {
	for (let i = 0; i < arr.length; i++) {
		for (let j = 0; j < arr.length - 1; j++) {
			if (arr[j] > arr[j+1]) {
				swap(arr, j, j+1);
			}
		}
	}
	return arr;
}

// prevent unnecessary comparisons with already sorted items.
function bubbleSortOptimized(arr) {
	let noSwaps = false; // if array is almost sorted, break out of loop to avoid unnecessary work.
	for (let i = arr.length - 1; i >= 0; i--)	{
		noSwaps = true
		for (let j = 0; j < i; j++) {
			if (arr[j] > arr[j+1]) {
				swap(arr, j, j+1);
				noSwaps = false;
			}
		}
		if (noSwaps) break;
	}
	return arr;
}

console.log(bubbleSortOptimized([7,1,2,3,4,6]));

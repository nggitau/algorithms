function longestPalindromeSubseq(s) {
	const dp = []
	const n = s.length

	for (let i = 0; i < n; i++) {
		dp.push(new Array(n).fill(0))
	}

	for (let i = 0; i < n; i++) {
		dp[i][i] = 1
	}

	const helper = (i, j) => {
		if (!s[i] || !s[j]) {
			return 0
		}

		if (s[i] === s[j]) {
			if (!dp[i][j]) {
				dp[i][j] = 1 + helper(i+1, j-1)
			}
			return dp[i][j]
		}
		if (!dp[i][j]) {
			dp[i][j] = Math.max(helper(i+1, j), helper(i, j-1))
		}
		return dp[i][j]
	}

	helper(0, s.length-1)
	console.log(dp)
	return dp[0][n-1]
};

// let x = longestPalindromeSubseq(`
// ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg`)
// console.log(x)
console.log(longestPalindromeSubseq("bbbab"))


// const helper = (i, j) => {
// 	if (!s[i] || !s[j]) {
// 		return 0
// 	}

// 	if (s[i] === s[j]) {
// 		if (!memo[i][j]) {
// 			memo[i][j] = 1 + helper(i+1, j-1)
// 		}
// 		return memo[i][j]
// 	}

// 	if (!memo[i][j]) {
// 	   memo[i][j] = Math.max(helper(i+1, j), helper(i, j-1))
// 	}
// 	return memo[i][j]
// }

// helper(0, s.length-1)
// console.log(memo)
// return memo[0][s.length-1]

// const n = s.length
// 	const memo = []
//     for (let i = 0; i < n; i++) {
//         memo.push(new Array(n).fill(0))
// 	}

// 	// fill for trivial case
// 	for (let i = 0; i < n; i++) {
// 		memo[i][i] = 1
// 	}

// 	for (let currLen = 2; currLen < n; currLen++) {
// 		for (let i = 0; i < n - currLen; i++) {
// 			const j = i + currLen
// 			if (s[i] === s[j]) {
// 				memo[i][j] = 2 + memo[i+1][j-1]
// 			} else {
// 				memo[i][j] = Math.max(memo[i+1][j], memo[i][j-1])
// 			}
// 		}
// 	}

// 	console.log(memo)
// 	return memo[0][n-1]

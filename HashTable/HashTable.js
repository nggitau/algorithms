class HashTable {
    constructor(size = 53) {
        this.keyMap = new Array(size)
    }

    _hash(key) {
        let total = 0
        let WEIRD_PRIME = 31

        for (let i = 0; i < Math.min(key.length, 100); i++) {
            let char = key[i]
            let value = char.charCodeAt(0) - 96
            total += (total + WEIRD_PRIME + value) % this.keyMap.length
        }
        return total % this.keyMap.length
    }

    // [x, x, x, [[k, v], [k, v]], x]

    set(key, value) {
        const index = this._hash(key)
        if (this.keyMap[index]) {
            const nameAlreadyExists = this.keyMap[index].some(([k, v]) => k === key)
            if (nameAlreadyExists) {
                // for now duplicate keys are ignored
                // would be ideal to overwrite their value with new value
                return
            }
            this.keyMap[index].push([key, value])
        }
        this.keyMap[index] = [[key, value]]
        return true
    }

    get(key) {
        const index = this._hash(key)

        if (!this.keyMap[index]) {
            return
        }
        // [[k, v]]
        return this.keyMap[index].find(([k, v]) => k === key)[1]
    }

    keys() {
        if (this.keyMap.length === 0) {
            return
        }
        const keys = []
        for (let i = 0; i < this.keyMap.length; i++) {
            if (this.keyMap[i]) {
                if (Array.isArray(this.keyMap[i])) {
                    for (let item of this.keyMap[i]) {
                        keys.push(item[0])
                    }
                }
            }
        }
        return keys
    }

    values() {
        if (this.keyMap.length === 0) {
            return
        }
        const dictValues = []
        for (let i = 0; i < this.keyMap.length; i++) {
            if (Array.isArray(this.keyMap[i])) {
                for (let item of this.keyMap[i]) {
                    dictValues.push(item[1])
                }
            }
        }
        return dictValues
    }
}

const hashTable = new HashTable()

function base62Encode(deci) {
	const codex = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
	let hashStr = ''

	while (deci > 0) {
		hashStr = codex[Math.floor(deci % 62)] + hashStr
		deci /= 62
	}

	return hashStr
}

console.log(base62Encode(1))

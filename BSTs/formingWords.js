/**
 * Given a set of positive numbers, find all possible combinations of words formed by replacing the
 * continuous digits with corresponding character of English alphabet.
 * i.e. subset {1} can be replaced by A, {2} can be replaced by B, {1, 0} can be replaced J,
 * {2, 1} can be replaced U, etc..
 *
 * @param {Array} numArray Array of numbers
 */
function formWords(numArray) {
	const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	const n = numArray.length
	const result = []

	const helper = (numArr, i, n, str) => {
		if (i === n) {
			result.push(str)
			return
		}
		let sum = 0
		for (let j = i; j < n; j++) {
			sum = (sum * 10) + numArr[j]
			if (sum > 0 && sum <= 26) {
				helper(numArr, j + 1, n, str + alphabet[sum - 1])
			}
		}
	}
	helper(numArray, 0, n, '')
	return result
}
const words = formWords([1,2,2,1])
console.log(words)

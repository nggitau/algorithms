/**
 * Given a binary tree, imagine yourself standing on the left side of it, return the values of
 * the nodes you can see ordered from top to bottom.
 *
 * @param {*} root
 */
function leftSideViewRecursive(root) {
	const result = []
	const traverse = (root, level, result) => {
		if (!root) {
			return
		}
		if (result[level] === undefined) {
			result[level] = [root.val]
		}
		traverse(root.left, level+1, result)
		traverse(root.right, level+1, result)
	}
	return result
}

function leftSideViewIterative(root) {
	const result = []
	if (!root) {
		return result
	}
	const queue = [root]
	while (queue.length) {
		const numNodesAtLevel = queue.length
		for (let i = 0; i < numNodesAtLevel; i++) {
			const curr = queue.shift()
			if (curr.left) {
				queue.push(curr.left)
			}
			if (curr.right) {
				queue.push(curr.right)
			}
			if (i === 0) {
				result.push(curr.val)
			}
		}
	}
	return result
}

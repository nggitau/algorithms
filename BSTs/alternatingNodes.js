/**
 * Print Nodes level by level, alternating left and right
 *
 * @param {*} root
 */
function alternatingNodes(root) {
	const result = []
	if (!root) {
		return result
	}
	result.push(root.val)
	const queue1 = []
	const queue2 = []
	if (root.left) {
		queue1.push(root.left)
	}
	if (root.right) {
		queue2.push(root.right)
	}
	while (queue1.length) {
		const numNodesAtLevel = queue1.length
		for (let i = 0; i < numNodesAtLevel; i++) {
			const curr1 = queue1.shift()
			result.push(curr1.val)
			if (curr1.left) {
				queue1.push(curr1.left)
			}
			if (curr1.right) {
				queue1.push(curr1.right)
			}
			const curr2 = queue2.shift()
			result.push(curr2.val)
			if (curr2.right) {
				queue2.push(curr2.right)
			}
			if (curr2.left) {
				queue2.push(curr2.left)
			}
		}
	}
	return result
}

/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var levelOrderBottom = function(root) {
    const result = []
    if (!root) {
        return result
    }
    result.push(root.val)
    const queue1 = []
    const queue2 = []
    if (root.left) {
        queue1.push(root.left)
    }
    if (root.right) {
        queue2.push(root.right)
    }
    while (queue1.length) {
        const numNodesLevel = queue1.length
        for (let i = 0; i < numNodesLevel; i++) {
            const curr1 = queue1.shift()
            result.push(curr1.val)
            if (curr1.left) {
                queue1.push(curr1.left)
            }
            if (curr1.right) {
                queue1.push(curr1.right)
            }

            const curr2 = queue2.shift()
            result.push(curr2.val)
            if (curr2.right) {
                queue2.push(curr2.right)
            }
            if (curr2.left) {
                queue2.push(curr2.left)
            }
        }
    }
    console.log(result)
    // return result
};

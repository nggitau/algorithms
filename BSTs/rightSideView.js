/**
 * Given a binary tree, imagine yourself standing on the right side of it, return the values of
 * the nodes you can see ordered from top to bottom.
 *
 * @param {*} root
 */
function rightSideViewRecursive(root) {
	const result = []
	const traverse = (root, level, result) => {
		if (!root) {
			return
		}
		if (result[level] === undefined) {
			result[level] = [root.val]
		} else {
			result[level].push(root.val)
		}
		traverse(root.left, level+1, result)
		traverse(root.right, level+1, result)
	}
	traverse(root)
	return result.map(level => level[level.length - 1])
}

function rightSideViewIterative(root) {
	const result = []
	if (!root) {
		return result
	}
	const queue = [root]
	while (queue.length) {
		const numNodesAtLevel = queue.length
		for (let i = 0; i < numNodesAtLevel; i++) {
			const curr = queue.shift()
			if (curr.left) {
				queue.push(curr.left)
			}
			if (curr.right) {
				queue.push(curr.right)
			}
			if (i === numNodesAtLevel - 1) {
				result.push(curr.val)
			}
		}
	}
	return result
}

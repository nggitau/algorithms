/**
 * Return true if and only if the nodes corresponding to the values x and y are cousins, else false
 *
 * @param {BT} root
 * @param {number} x
 * @param {number} y
 */
function checkIfCousins(root, x, y) {
	if (!root) return
	const xDetails = {}
	const yDetails = {}
	const traverse = (root, parent, level, xDetails, yDetails) => {
		if (!root) {
			return
		}
		if (root.val === x) {
			xDetails['level'] = level
			xDetails['parent'] = parent
		}
		if (root.val === y) {
			yDetails['level'] = level
			yDetails['parent'] = parent
		}
		traverse(root.left, root, level+1, xDetails, yDetails)
		traverse(root.right, root, level+1, xDetails, yDetails)
	}
	traverse(root, null, 0, xDetails, yDetails)
	if (xDetails.level !== yDetails.level) {
		return false
	}
	if ((xDetails.parent && xDetails.parent.val) === (yDetails.parent && yDetails.parent.val)) {
		return false
	}
	return true
}

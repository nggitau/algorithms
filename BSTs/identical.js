const BST = require('./BST')

/**
 * Check if 2 binary trees are identical or not.
 *
 * @param t1
 * @param t2
 */
function checkIdenticalRecursive(t1, t2) {
	if (!t1 && !t2) {
		return true
	}
	if ((t1 && !t2) || (!t1 && t2)) {
		return false
	}
	if (t1.val !== t2.val) {
		return false
	}
	return checkIdenticalRecursive(t1.left, t2.left) && checkIdenticalRecursive(t1.right, t2.right)
}

function checkIdenticalIteratice(p, q) {
	if (!p && !q) {
		return true
	}
	if (!p || !q) {
		return false
	}
	const stack = []
	stack.push([p, q])
	while (stack.length) {
		const [t1, t2] = stack.pop()
		if (t1.val !== t2.val) {
			return false
		}
		if (t1.left && t2.left) {
			stack.push([t1.left, t2.left])
		} else if ((t1.left && !t2.left) || (!t2.left && t2.left)) {
			return false
		}
		if (t1.right && t2.right) {
			stack.push([t1.right, t2.right])
		} else if ((t1.right && !t2.right) || (!t1.right && t2.right)) {
			return false
		}
	}
	return true
}

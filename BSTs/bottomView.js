/**
 * Given a binary tree, imagine yourself standing on the bottom side of it, return the values of
 * the nodes you can see.
 *
 * @param {*} root
 */
function bottomView(root) {
	const levelOrderResult = []
	const traverse = (root, level, horizontal, levelOrderResult) => {
		if (!root) {
			return
		}
		if (levelOrderResult[level] === undefined) {
			levelOrderResult[level] = [{ horizontal, val: root.val }]
		} else {
			levelOrderResult[level].push({ horizontal, val:root.val })
		}
		traverse(root.left, level+1, horizontal-1, levelOrderResult)
		traverse(root.right, level+1, horizontal+1, levelOrderResult)
	}
	traverse(root, 0, 0, levelOrderResult)
	const flattenedLevelOrder = levelOrderResult.flat()
	const bottomViewObj = {}
	flattenedLevelOrder.forEach(
		node => bottomViewObj[node.horizontal]
			? bottomViewObj[node.horizontal].push(node.val)
			: bottomViewObj[node.horizontal] = [node.val])
	// for top view we need to return val[0] instead of val[val.length-1]
	return Object.values(bottomViewObj).map(val => val[val.length - 1])
}

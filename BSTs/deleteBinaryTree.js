/**
 * Given a binary tree, write an efficient algorithm to delete entire binary tree.
 *
 * @param root
 */
function deleteBinaryTreeRecursive(root) {
	// delete leaf nodes first, then parents, all the way up to root
	if (!root) {
		return
	}
	deleteBinaryTreeRecursive(root.left)
	deleteBinaryTreeRecursive(root.right)
	// after deleting left and right subtrees, delete current
	delete root
	// set root to null
	root = null
}

function deleteBinaryTreeIterative(root) {
	if (!root) {
		return
	}
	const queue = []
	queue.push(root)
	while (queue.length) {
		const curr = queue.shift()
		if (curr.left) {
			queue.push(curr.left)
		}
		if (curr.right) {
			queue.push(curr.right)
		}
		// delete curr only after queueing its children
		delete curr
	}
	root = null
}

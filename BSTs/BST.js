class Node {
  constructor(val) {
      this.val = val
      this.left = null
      this.right = null
  }
}

class BST {
  constructor() {
      this.root = null
  }

  insert(val) {
      const newNode = new Node(val)
      let currentRoot = this.root
      if (!this.root) {
          this.root = newNode
          return this.root
      }

      while (true) {
          if (val < currentRoot.val) {
              // insert left
              if (currentRoot.left === null) {
                  currentRoot.left = newNode
                  return this
              } else {
                  currentRoot = currentRoot.left
              }
          } else if (val > currentRoot.val) {
              // insert right
              if (currentRoot.right === null) {
                  currentRoot.right = newNode
                  return this
              } else {
                  currentRoot = currentRoot.right
              }

          } else {
              return 'Value already in tree'
          }
      }

      return this.root
  }

  find(val) {
      if(!this.root) {
          return
      }
      if (this.root.val === val) {
          return this.root
      }
      let current = this.root
      while (true) {
          if (val < current.val) {
              current = current.left
              if (current === null) {
                  return false
              }
              if (val === current.val) {
                  return current
              }
          } else if (val > current.val) {
              current = current.right
              if (current === null) {
                  return false
              }
              if (val === current.val) {
                  return current
              }
          } else {
              return false
          }
      }
  }

  traverseBFS() {
      if (!this.root) {
          return
      }
      const queue = []
      const visited = []

      queue.push(this.root)

      while(queue.length > 0) {
          const front = queue.shift()
          visited.push(front)
          if(front.left) {
              queue.push(front.left)
          }
          if (front.right) {
              queue.push(front.right)
          }
      }

      return visited.map(n => n.val)
  }

  // root, left, right
  DFSPreOrder() {
      const visited = []

      function traverse(node) {
          if (!node) {
              return
          }

          visited.push(node)

          if (node.left) {
              traverse(node.left)
          }
          if (node.right) {
              traverse(node.right)
          }
      }

      traverse(this.root)
      return visited.map(n => n.val)
  }

  // left, right, root
  DFSPostOrder () {
      const visited = []

      function traverse(node) {
          if (!node) {
              return
          }

          if (node.left) {
              traverse(node.left)
          }

          if (node.right) {
              traverse(node.right)
          }

          visited.push(node)
      }

      traverse(this.root)
      return visited.map(n => n.val)
  }

  // left, root, right
  DFSInOrder() {
      const visited = []

      function traverse(node) {
          if (!node) {
              return
          }

          if (node.left) {
              traverse(node.left)
          }

          visited.push(node)

          if (node.right) {
              visited.push(node.right)
          }
      }

      traverse(this.root)
      return visited.map(v => v.val)
  }
}

const t = new BST()
module.exports = t

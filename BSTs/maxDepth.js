/**
 * Calculate the maximum depth of a binary tree.
 * The maximum depth is the number of nodes along the longest path from the root node down to
 * the farthest leaf node.
 *
 * @param root
 */
function maxDepthRecursive(root) {
	if (!root) {
		return 0
	}
	return 1 + Math.max(maxDepthRecursive(root.left), maxDepthRecursive(root.right))
}

function maxDepthIterative(root) {
	// empty tree has a height of 0
	if (!root) {
		return 0
	}
	let height = 0
	const queue = []
	queue.push(root)
	while (queue.length) {
		// num nodes at current level
		const numNodes = queue.length
		for (let i = 0; i < numNodes; i++) {
			const curr = queue.shift()
			if (curr.left) {
				queue.push(curr.left)
			}
			if (curr.right) {
				queue.push(curr.right)
			}
		}
		height++
	}
	return height
}

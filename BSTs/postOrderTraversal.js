/**
 * Post order BST traversal, Left, Right, Root
 * @param {*} root
 */
function postOrderTraversalRecursive(root) {
	const result = []
	if (!root) {
		return result
	}
	const traverse = (root) => {
		if (root.left) {
			traverse(root.left)
		}
		if (root.right) {
			traverse(root.right)
		}
		result.push(root.val)
	}
	traverse(root)
	return result
}

function postOrderTraversalIterative(root) {
	const result = []
	if (!root) {
		return result
	}
	const postOrderStack = []
	const stack = []
	stack.push(root)
	while (stack.length) {
		const curr = stack.pop()
		postOrderStack.push(curr)
		if (curr.left) {
			stack.push(curr.left)
		}
		if (curr.right) {
			stack.push(curr.right)
		}
	}
	while (postOrderStack.length) {
		const curr = postOrderStack.pop()
		result.push(curr.val)
	}
	return result
}

/**
 * Given a binary tree, return a list of its inorder traversal of nodes
 *
 * @param root
 * @return {Number[]}
 */
function inOrderRecursive(root) {
	const result = []
	if (!root) {
		return result
	}
	const traverse = (root) => {
		if (root.left) {
			traverse(root.left)
		}
		result.push(root.val)
		if (root.right) {
			traverse(root.right)
		}
	}
	traverse(root)
	return result
}

function inOrderIterative(root) {
	const result = []
	if (!root) {
		return result
	}
	const stack = []
	let curr = root
	while (stack.length || curr) {
		if (curr) {
			stack.push(curr)
			curr = curr.left
		} else {
			const node = stack.pop()
			result.push(node.val)
			curr = node.right
		}
	}
	return result
}

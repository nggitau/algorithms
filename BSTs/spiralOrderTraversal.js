/**
 * Given a binary tree, print its nodes level by level in spiral order. i.e. all nodes present at
 * level 1 should be printed first from left to right, followed by nodes of level 2 right to left,
 * followed by nodes of level 3 from left to right and so on.
 *
 * @param {*} root
 */
function spiralOrderTraversalRecursive(root) {
	const result = []
	const traverse = (root, level, result) => {
		if (!root) {
			return null
		}
		if (result[level] === undefined) {
			result[level] = [root.val]
		} else if (level % 2 === 0) {
			result[level].push(root.val)
		} else if (level % 2 !== 0) {
			result[level] = [root.val, ...result[level]]
		}
		traverse(root.left, level+1, result)
		traverse(root.right, level+1, result)
	}
	traverse(root, 0, result)
	return result
}

function spiralOrderTraversalIterative(root) {
	const result = []
	if (!root) {
		return result
	}
	const queue = []
	let leftToRight = true
	queue.push(root)
	while (queue.length) {
		// number of nodes at current level
		const numNodesAtLevel = queue.length
		const nodesAtLevel = []
		for (let i = 0; i < numNodesAtLevel; i++) {
			const curr = queue.shift()
			nodesAtLevel.push(curr.val)
			if (curr.left) {
				queue.push(curr.left)
			}
			if (curr.right) {
				queue.push(curr.right)
			}
		}
		if (leftToRight) {
			result.push(nodesAtLevel)
		} else {
			result.push(nodesAtLevel.reverse())
		}
		leftToRight = !leftToRight
	}
	return result
}

/**
 * Check if Binary Tree is complete Binary Tree or not
 *
 * @param {*} root
 */
function checkComplete(root) {
	if (!root) {
		return true
	}
	const queue = [root]
	let fullNode = true
	while (queue.length) {
		const curr = queue.shift()
		if (!fullNode && (curr.left || curr.right)) {
			return false
		}
		if (curr.right && !curr.left) {
			return false
		}
		if (curr.left) {
			queue.push(curr.left)
		} else {
			fullNode = false
		}
		if (curr.right) {
			queue.push(curr.right)
		} else {
			fullNode = false
		}
	}
	return true
}

function checkCompleteRecursive(root) {
	const serialized = []
	const traverse = (root, index, serialized) => {
		if (!root) {
			return
		}
		traverse(root.left, index*2+1, serialized)
		serialized[index] = root.val
		traverse(root.right, index*2+2, serialized)
	}
	traverse(root, 0, serialized)
	for (let item of serialized) {
		if (item === undefined) {
			return false
		}
	}
	return true
}

function checkCompleteRecursiveNoExtraSpace(root) {
	const size = (root) => {
		if (!root) return 0
		return 1 + size(root.left) + size(root.right)
	}
	const traverse = (root, index, n) => {
		if (!root) return true
		if ((root.left && 2*index + 1 >= n) || !traverse(root.left, 2*index+1, n)) {
			return false
		}
		if ((root.right && 2*index + 2 >= n) || !traverse(root.right, 2*index+2, n)) {
			return false
		}
		return true
	}
	return traverse(root, 0, size(root))
}

/**
 * Level Order Traversal of a Binary Tree
 *
 * @param {*} root
 */
function levelOrderTraversalRecursive(root) {
	const result = []
	const traverse = (root, level, result) => {
		if (!root) {
			return null
		}
		if (result[level] === undefined) {
			result[level] = [root.val]
		} else {
			result[level].push(root.val)
		}
		traverse(root.left, level+1, result)
		traverse(root.right, level+1, result)
	}
	traverse(root, 0, result)
	return result
}

function levelOrderTraversalIterative(root) {
	const result = []
	if (!root) {
		return result
	}
	const queue = []
	queue.push(root)
	while (queue.length) {
		// number of nodes at current level
		const numNodesLevel = queue.length
		const currLevelNodes = []
		for (let i = 0; i < numNodesLevel; i++) {
			const curr = queue.shift()
			currLevelNodes.push(curr.val)
			if (curr.left) {
				queue.push(curr.left)
			}
			if (curr.right) {
				queue.push(curr.right)
			}
		}
		result.push(currLevelNodes)
	}
	return result
}

/**
 * Check if tree root is a Sum Tree
 *
 * @param {BT} root The Binary Tree to check
 */
function checkIfSumTree(root) {
	const isSumTree = (root) => {
		if (!root) {
			return 0
		}

		if (root.val === checkIfSumTree(root.left) + checkIfSumTree(root.right)) {
			return 2*root.val
		}

		return Number.MIN_VALUE
	}
	const result = isSumTree(root)
	if (result === Number.MIN_VALUE) {
		return false
	}
	return true
}

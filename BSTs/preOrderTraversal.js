/**
 * Preorder traversal of Binary Tree. // Root, Left, Right
 *
 * @param {*} root
 * @return {Number[]}
 */
function preOrderTraversalRecursive(root) {
	const result = []
	if (!root) {
		return result
	}
	const traverse = (root) => {
		result.push(root.val)
		if (root.left) {
			traverse(root.left)
		}
		if (root.right) {
			traverse(root.right)
		}
	}
	traverse(root)
	return result
}

function preOrderTraversalIterative(root) {
	const result = []
	if (!root) {
		return result
	}
	const stack = []
	stack.push(root)
	while (stack.length) {
		const curr = stack.pop()
		result.push(curr.val)
		// we need to push right first so left will be processed first LIFO
		if (curr.right) {
			stack.push(curr.right)
		}
		if (curr.left) {
			stack.push(curr.left)
		}
	}
	return result
}

/**
 * Check if t is a subtree of s
 *
 * @param {BT} s
 * @param {BT} t
 */
function checkIfSubTree(s, t) {
	let subTreeFound = false
	const isSubTree = (s, t) => {
		if (!s && !t) return true
		if (!s || !t) return false
		if (s.val !== t.val) return false
		isSubTree(s.left, t.left)
		isSubTree(s.right, t.right)
	}
	const findCommonNode = (s, t) => {
		if (!s && !t) return false
		if (!s || !t) return false
		if (s.val === t.val) {
			const match = isSubTree(s, t)
			subTreeFound = subTreeFound || match
		}
		return findCommonNode(s.left, t) || findCommonNode(s.right, t)
	}
	findCommonNode(s, t)
	return subTreeFound
}

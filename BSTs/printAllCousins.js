/**
 * Print all cousins of the node x
 *
 * @param {BT} root
 * @param {number} x
 * @return {number[]}
 */
function printAllCousins(root, x) {
	// 1. find the level and parent of x
	let parent = null
	let level = null
	const findLevelAndParent = (root, currNode, currLevel, x) => {
		if (!root) {
			return
		}
		if (root.val === x) {
			level = currLevel
			parent = currNode
			return
		}
		findLevelAndParent(root.left, root, currlevel+1, x)
		findLevelAndParent(root.right, root, currLevel+1, x)
	}
	findLevelAndParent(root, null, 0, x)
	// 2. traverse tree and return nodes in same level but different parents from x
	const cousins = []
	const traverse = (root, parent, level, xParent, xLevel, x, cousins) => {
		if (!root) {
			return
		}
		if ((level === xLevel) && (parent !== xParent)) {
			cousins.push(root.val)
		}
		traverse(root.left, root, level + 1, xParent, xLevel, x, cousins)
		traverse(root.right, root, level + 1, xParent, xLevel, x, cousins)
	}
	traverse(root, null, 0, parent, level, x, cousins)
	return cousins
}

/**
 * Given a binary tree, write an efficient algorithm to check if it is symmetric binary tree or not.
 * The tree has symmetric structure if left subtree and right subtree are mirror images of each
 * other.
 *
 * @param {BST} root
 */
function isSymmetricRecursive(root) {
	const helper = (A, B) => {
		if (!A && !B) {
			return true
		}
		if (!A || !B) {
			return false
		}
		return (A.val === B.val)
			&& isSymmetric(A.left, B.right)
			&& isSymmetric(A.right, B.left)
	}
	if (!root) {
		return true
	}
	return helper(root.left, root.right)
}

function isSymmetricIterative(root) {
	// level order traversal

}

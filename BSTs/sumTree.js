/**
 * Given a binary tree, in-place convert it to its sum tree. In a sum tree, value at each node
 * is equal to the sum of all elements present in its left and right subtree. The value of an
 * empty node is considered as 0.
 *
 * @param {*} root
 */
function sumTree(root) {
	const helper = (root) => {
		if (!root) {
			return 0
		}
		const left = helper(root.left)
		const right = helper(root.right)
		const toReturn = root.val + left + right
		root.val = left + right
		return toReturn
	}
	helper(root)
	return root
}

/**
 * Calculate the diameter of a Binary Tree
 *
 * @param {BST} root
 */
function diameterOfBinaryTree(root) {
	let diameter = 0
	const getDiameter = (root) => {
		if (!root) return 0
		const left = getDiameter(root.left)
		const right = getDiameter(root.right)
		const maxDiameter = 1 + Math.max(left, right)
		diameter = Math.max(left + right, diameter)
		return maxDiameter
	}
}

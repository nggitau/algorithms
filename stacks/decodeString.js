/**
 * @param {string} s
 * @return {string}
 */
var decodeString = function(s) {
    const stack = []
    let index = 0

    while (index < s.length) {
        if (s[index] !== ']') {
            stack.push(s[index])
        } else {
            let temp = []
            let current = stack.pop()
            temp.push(current)
            while (true) {
                current = stack.pop()
                if (current !== '[') {
                    temp.push(current)
                } else {
                    break
                }
            }

            let reps = []
            while(true) {
                num = stack[stack.length - 1]
                if (!isNaN(num)) {
					stack.pop()
                    reps.push(num)
                } else {
                    break
                }
            }

            reps = Number(reps.reverse().join(''))
            temp = temp.reverse()
            let temps = ''

            for (let i = 0; i < reps; i++) {
                temps += temp.join('')
            }
            stack.push(temps)

        }
        index++
    }

    return stack.join('')
};

console.log(decodeString('2[abc]3[cd]ef'))

class Node {
  constructor(val) {
      this.val = val
      this.next = null
  }
}

class Stack {
  constructor() {
      this.size = 0
      this.head = null
  }

  push(val) {
      const newNode = new Node(val)
      if (!this.head) {
          this.head = newNode
      } else {
          const prevHead = this.head
          newNode.next = prevHead
          this.head = newNode
      }
      this.size++
      return this.size
  }

  pop() {
      const oldHead = this.head
      if (!oldHead) {
          return null
      } else {
          this.head = oldHead.next
          oldHead.next = null
      }
      this.size--
      return oldHead
  }
}

const stack = new Stack()

function mergeLists(head1, head2) {
  if (!head1) {
    return head2
  }
  if (!head2) {
    return head1
  }
  if (!head1 && !head2) {
    return
  }
  let head3 = null

  if (head1.data < head2.data) {
    head3 = head1
    head1 = head1.next
  } else {
    head3 = head2
    head2 = head2.next
  }

  let currentNode = head3

  while(head1 && head2) {
    if (head1.data < head2.data) {
      currentNode.next = head1
      head1 = head1.next
    } else {
      currentNode.next = head2
      head2 = head2.next
    }
    currentNode = currentNode.next
  }

  if (!head1) {
    currentNode.next = head2
  } else if (!head2) {
    currentNode.next = head1
  }

  return head3
}

class ListNode {
  constructor(val) {
      this.val = val
      this.next = null
  }
}

class SinglyLinkedList {
  constructor() {
      this.head = null
      this.tail = null
      this.length = 0
  }

  push(val) {
      const newNode = new ListNode(val)
      if (!this.head && !this.tail) {
          this.head = newNode
          this.tail = newNode
      }
      this.tail.next = newNode
      this.tail = newNode
      this.length++
      return this
  }

  pop() {
      // remove the last item in the linked list
      if (!this.head) return undefined
      let current = this.head
      let newTail = this.tail

      while(current.next) {
          newTail = current
          current = current.next
      }
      this.tail = newTail
      this.tail.next = null
      this.length--
      if (this.length === 0) {
          this.head = null
          this.tail = null
      }
      return current
  }

  shift() {
      // remove from front
      if (this.head === null) {
          return undefined
      }
      const prevHead = this.head
      this.head = prevHead.next
      this.length--
      if (this.length === 0) {
          this.tail = this.head
      }
      return prevHead
  }

  unshift(val) {
      const newNode = new ListNode(val)
      if (!this.head) {
          this.head = newNode
          this.tail = this.head
      } else {
          newNode.next = this.head
          this.head = newNode
      }

      this.length++
      return this
  }

  get(index) {
      if (!this.head || index < 0 || index >= this.length) {
          return undefined
      }

      let currentNode = this.head
      let counter = 0

      while(counter !== index) {
          counter++
          currentNode = currentNode.next
      }
      return currentNode
  }

  set(index, newVal) {
      const foundNode = this.get(index)
      if (foundNode) {
          foundNode.val = newVal
          return true
      }
      return false
  }

  insert(index, val) {
      if (index > this.length || index < 0) {
          return false
      }
      const newNode = new ListNode(val)
      if (index === 0) {
          this.unshift(val)
          return true
      }
      if (index === this.length) {
          this.push(val)
          return true
      }
      const prevNode = this.get(index - 1)
      const current = this.get(index)

      prevNode.next = newNode
      newNode.next = current
      this.length++
      return true
  }

  remove(index) {
      if (index > this.length || index < 0) {
          return undefined
      }
      if (index === this.length - 1) {
          this.pop(index)
          return true
      }
      if (index === 0) {
          this.shift()
          return true
      }
      const prevNode = this.get(index - 1)
      const current = prevNode.next
      prevNode.next = current.next
      this.length--
      return true
  }

  print() {
      const arr = []
      let current = this.head
      while (current) {
          arr.push(current.val)
          current = current.next
      }
      console.log(arr)
  }

  reverse() {
      let node = this.head
      this.head = this.tail
      this.tail = node
      let prev = null
      let next

      while (node) {
          next = node.next
          node.next = prev
          prev = node
          node = next
      }
      return this
  }
}

const list = new SinglyLinkedList()
list.push(1)
list.push(2)
list.push(3)
list.push(4)
list.push(5)

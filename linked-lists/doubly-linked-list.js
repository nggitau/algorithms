class Node {
  constructor(val) {
      this.val = val
      this.next = null
      this.prev = null
  }
}

class DoublyLinkedList {
  constructor() {
      this.head = null
      this.tail = null
      this.length = 0
  }

  print() {
      const list = []
      let current = this.head
      while (current) {
          list.push(current.val)
          current = current.next
      }
      console.log(list)
  }

  push(val) {
      const newNode = new Node(val)
      if (!this.head) {
          this.head = newNode
          this.tail = this.head
      } else {
          this.tail.next = newNode
          newNode.prev = this.tail
          this.tail = newNode
      }
      this.length ++
      return this
  }

  pop() {
      if (!this.tail) {
          return undefined
      }

      const currentLast = this.tail

      if (this.length === 1) {
          this.head = null
          this.tail = null
      } else {
          this.tail = currentLast.prev
          this.tail.next = null
      }

      this.length --
      currentLast.prev = null
      return currentLast
  }

  shift() {
      if (!this.head) {
          return undefined
      }

      const shiftedNode = this.head

      if (this.length === 1) {
          this.tail = null
          this.head = null
      } else {
          this.head = shiftedNode.next
          this.head.prev = null
      }

      this.length --
      shiftedNode.next = null
      return shiftedNode
  }

  unshift(val) {
      const newNode = new Node(val)
      if (!this.head) {
          this.head = newNode
          this.tail = newNode
      } else {
         this.head.prev = newNode
         newNode.next = this.head
         this.head = newNode
      }
      this.length++
      return this
  }

  get(index) {
      if (index < 0 || index >= this.length) {
          return undefined
      }
      if (index > this.length / 2) {
          let counter = this.length - 1
          let current = this.tail
          while(counter !== index) {
              current = current.prev
              counter --
          }
          return current
      } else {
          let counter = 0
          let current = this.head

          while(counter !== index) {
              counter++
              current = current.next
          }
          return current
      }
  }

  remove(index) {
      if (index === 0) {
          return this.shift()
      }
      if (index === this.length - 1) {
          return this.pop()
      }

      const nodeToRemove = this.get(index)
      const beforeNode = this.get(index - 1)
      const afterNode = this.get(index + 1)

      nodeToRemove.next = null
      nodeToRemove.prev = null

      beforeNode.next = afterNode
      afterNode.prev = beforeNode
      this.length --
      return nodeToRemove

  }

  replace(index, val) {
      const foundNode = this.get(index)
      if (foundNode) {
          foundNode.val = val
      }
      return foundNode
  }
}

const list = new DoublyLinkedList()

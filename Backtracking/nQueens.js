function canPlace(bo, row, col) {
	// if col is attacked, return false;
	for (let i = 0; i < row; i++) {
		if (bo[i][col] === 'Q') return false
	}

	// if \ diagonal is attacked return false
	for (let i = row, j = col; i >= 0 && j >= 0; i--, j--) {
		if (bo[i][j] === 'Q') return false
	}

	// if / diagonal is attacked, return false
	for (let i = row, j = col; i >= 0 && j < bo.length; i--, j++) {
		if (bo[i][j] === 'Q') return false
	}

	return true
}

function solveNQueens(n) {
	// initialize n * n board
	const bo = Array.from(Array(n), () => Array(n).fill('.'))
	let numSols = 0
	// recursive helper function to solve nqueens
	const helper = (bo, row) => {
		if (row === bo.length) {
			numSols++
			console.log(bo)
		}
		for (let i = 0; i < bo.length; i++) {
			if (canPlace(bo, row, i)) {
				bo[row][i] = 'Q'
				helper(bo, row+1)
				bo[row][i] = '.'
			}
		}
	}
	helper(bo, 0)
	return numSols
}

solveNQueens(4)


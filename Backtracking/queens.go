package main

import (
	"strings"
	"fmt"
)

func solveNQueens(n int) int {
    res := [][]string{}
    cols := make([]int, n)
	helper(n, 0, &res, cols)
    return len(res)
}

func helper(n int, row int, res *[][]string, cols []int) {
    // we've filled a correct combination
    if row == n {
        // build res
        curr := make([]string, n)
        for r, c := range cols {
            // build row
            var s strings.Builder
            for i := 0; i < n; i++ {
                if i == c {
                    s.WriteString("Q")
                } else {
                    s.WriteString(".")
                }
            }
            curr[r] = s.String()
        }
        *res = append(*res, curr)
    }
    // try to fill a slot in current row
    for col := 0; col < n; col++ {
        // check if possible to fill
        canFill := true
        for r := 0; r < row; r++ {
            c := cols[r]
            // check col
            if col == c {
                canFill = false
            }
            // check diagonals
            if row-r == col-c || row-r == (col-c)*-1 {
                canFill = false
            }
        }
        if canFill { // place queen at (row, col) and recurse
            cols[row] = col
            helper(n, row+1, res, cols)
        }
    }
}

func main () {
	sols := solveNQueens(11)
	fmt.Println(sols)
}

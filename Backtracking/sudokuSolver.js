let board = [
	["5","3",".",".","7",".",".",".","."],
	["6",".",".","1","9","5",".",".","."],
	[".","9","8",".",".",".",".","6","."],
	["8",".",".",".","6",".",".",".","3"],
	["4",".",".","8",".","3",".",".","1"],
	["7",".",".",".","2",".",".",".","6"],
	[".","6",".",".",".",".","2","8","."],
	[".",".",".","4","1","9",".",".","5"],
	[".",".",".",".","8",".",".","7","9"]
]




function findEmpty(bo) {
	for (let i = 0; i < bo.length; i++) {
		for (let j = 0; j < bo[0].length; j++) {
			if (bo[i][j] === '.') {
				return [i, j] // row, col
			}
		}
	}
	return false
}

function valid(bo, num, pos) {
	// check row
	for (let i = 0; i < bo[0].length; i++) {
		if (bo[pos[0]][i] == num && pos[1] != i) {
			return false
		}
	}

	// check column
	for (let i = 0; i < bo.length; i++) {
		if (bo[i][pos[1]] == num && pos[0] != i) {
			return false
		}
	}

	// check 3x3 cubes
	let boxX = Math.floor(pos[1] / 3)
	let boxY = Math.floor(pos[0] / 3)

	for(let i = boxY*3; i < boxY*3 + 3; i++) {
		for(let j = boxX*3; j < boxX*3 + 3; j++) {
			if (bo[i][j] == num && [i, j] != pos) {
				return false
			}
		}
	}

	return true
}

function solve(bo) {
	const empty = findEmpty(bo)
	if (!empty) { // board is full
		return bo
	}
	const [row, col] = empty
	for (let i = 1; i < 10; i++) {
		if (valid(bo, i, [row, col])) {
			bo[row][col] = '' + i
			if (solve(bo)) {
				return true
			} else {
				bo[row][col] = '.'
			}
		}
	}
	return false
}

console.log(board)

solve(board)


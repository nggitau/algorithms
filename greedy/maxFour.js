let board = [
	[1,2,3,4,5],
	[4,5,6,4,5],
	[11,0,4,5,6],
	[11,0,4,5,6],
	[11,0,4,5,6]
]

function findNeighboring(bo, row, col) {
	// neighboroing if they share a common side
}

function solution(bo) {
	let currentMax = -Infinity
	const path = [0]
	for (let row = 0; row < bo.length; row++) {
		for (let col = 0; col < bo[0].length; col++) {
			if (bo[row][col] > path[path.length - 1]) {
				path.push(bo[row][col])
			}
		}
	}
	return path
}

const sol = solution(board)
// console.log({sol})

let plane = [
	[1,0,1,0,0,0,0,0,0,0],
	[0,0,0,0,0,1,0,0,0,0]
]

function numFamilies(plane) {
	// find 4 consequtive empty seats in each row
	let families = 0
	let empty = 0
	for (let row = 0; row < plane.length; row++) {
		for (let col = 0; col < plane[0].length - 1; col++) {
			if (plane[row][col] === 0) {
				empty ++
				if (empty === 4) {
					families ++
					empty = 0
				}
			} else {
				empty = 0
			}
			if (col === 0) {
				empty = 0
			}
		}
		empty = 0
	}
	return families
}

const fams = numFamilies(plane)
console.log({fams})

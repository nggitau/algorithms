class MaxBinaryHeap {
  constructor() {
      this.values = []
  }

  insert(val) {
      this.values.push(val)

      let parentIndex = Math.floor(((this.values.length - 1) - 1) / 2)
      let currentIndex = this.values.length - 1

      while(this.values[parentIndex] < this.values[currentIndex]) {
          if (parentIndex === Infinity) {
              break
          }
          const tempParent = this.values[parentIndex]
          this.values[parentIndex] = this.values[currentIndex]
          this.values[currentIndex] = tempParent

          currentIndex = parentIndex
          parentIndex = Math.floor((currentIndex - 1) / 2)
      }

      return this.values
  }

  extractMax() {
      const prevMax = this.values[0]

      if (this.values.length === 1) {
          return this.values.pop()
      }

      if (this.values.length === 0) {
          return
      }

      const last = this.values.pop()
      this.values[0] = last
      let parentIndex = 0
      let leftChildIndex = 2 * parentIndex + 1
      let rightChildIndex = leftChildIndex + 1

      while(this.values[leftChildIndex] && this.values[rightChildIndex]) {
          const parentElement = this.values[parentIndex]
          const leftChild = this.values[leftChildIndex]
          const rightChild = this.values[rightChildIndex]


          if (leftChild && rightChild && leftChild > rightChild && leftChild > parentElement) {
              this.values[parentIndex] = leftChild
              this.values[leftChildIndex] = parentElement
              parentIndex = leftChildIndex
              leftChildIndex = 2 * parentIndex + 1
              rightChildIndex = leftChildIndex + 1
          }

          if (rightChild && leftChild && rightChild > leftChild && rightChild > parentElement) {
              this.values[parentIndex] = rightChild
              this.values[rightChildIndex] = parentElement
              parentIndex = rightChildIndex
              rightChildIndex = 2 * parentIndex + 2
              leftChildIndex = rightChildIndex - 1
          }

      }

      if (this.values.length === 2) {
          if (this.values[0] < this.values[1]) {
              const temp = this.values[0]
              this.values[0] = this.values[1]
              this.values[1] = temp
          }
      }
      return prevMax

  }
}

const maxHeap = new MaxBinaryHeap()
maxHeap.insert(55)
maxHeap.insert(39)
maxHeap.insert(41)
maxHeap.insert(18)
maxHeap.insert(27)
maxHeap.insert(12)
maxHeap.insert(33)

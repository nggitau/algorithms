class Node {
  constructor(value, priority) {
      this.value = value
      this.priority = priority
  }
}

class PriorityQueue {
  constructor() {
      this.queue = []
  }

  enqueue(value, priority) {
      const newNode = new Node(value, priority)

      if (this.queue.length === 0) {
          this.queue.push(newNode)
          return this.queue
      }

      this.queue.push(newNode)

      let childIndex = this.queue.length - 1
      let parentIndex = Math.floor((childIndex - 1) / 2)

      while (this.queue[parentIndex] && this.queue[childIndex]) {
          const parent = this.queue[parentIndex]
          const child = this.queue[childIndex]
          if (child.priority < parent.priority) {
              this.queue[parentIndex] = this.queue[childIndex]
              this.queue[childIndex] = parent
          }
          childIndex = parentIndex
          parentIndex = Math.floor((childIndex - 1) / 2)
      }

      return this.queue.map(q => q.priority)
  }


  dequeue() {
      if (this.queue.length === 1) {
          return this.queue.pop()
      }

      // remove from front
      // promote item at end to front
      // bubble down

      const oldFront = this.queue[0]
      const end = this.queue.pop()
      this.queue[0] = end

      let parentIndex = 0
      let leftChildIndex = 2 * parentIndex + 1
      let rightChildIndex = leftChildIndex + 1

      while(this.queue[leftChildIndex] && this.queue[rightChildIndex]) {
          const parent = this.queue[parentIndex]
          const leftChild = this.queue[leftChildIndex]
          const rightChild = this.queue[rightChildIndex]

          if (parent.priority > leftChild.priority && leftChild.priority < rightChild.priority) {
              this.queue[parentIndex] = leftChild
              this.queue[leftChildIndex] = parent
              parentIndex = leftChildIndex
          }

          if (parent.priority > rightChild.priority && rightChild.priority < leftChild.priority) {
              this.queue[parentIndex] = rightChild
              this.queue[rightChildIndex] = parent
              parentIndex = rightChildIndex
          }

          leftChildIndex = parentIndex * 2 + 1
          rightChildIndex = leftChildIndex + 1
      }

      if (this.queue.length === 2) {
          if (this.queue[0].priority > this.queue[1].priority) {
              let temp = this.queue[0]
              this.queue[0] = this.queue[1]
              this.queue[1] = temp
          }
      }

      return oldFront
  }
}

module.exports = PriorityQueue

const pq = new PriorityQueue()
// pq.enqueue('low fever', 4)
// pq.enqueue('high fever', 3)
// pq.enqueue('broken arm', 2)
// pq.enqueue('gunshot wound', 1)
// pq.enqueue('scratch', 5)
// pq.enqueue('broken skull', 0)

/**
 * Shuffle a given array of elements (Fisher–Yates shuffle)
 * @param A
 */
function unbiasedArrayShuffle(A: number[]) {
	for (let i = A.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i))
		swap(A, i, j)
	}
	return A
}

const swap = (A: number[], i: number, j: number): void => {
	const temp = A[i]
	A[i] = A[j]
	A[j] = temp
}

const A = [1,2,3,4,5]
console.log(A)
unbiasedArrayShuffle(A)
console.log(A)

export {}

/**
 * Sorts Binary array arr in linear time and constant space
 * @param {Array} arr
 */
function sortBinaryArr(arr) {
	let zerosCount = 0
	for (let item of arr) {
		if (item === 0) {
			zerosCount++
		}
	}
	for (let i = 0; i < arr.length; i++) {
		if (i < zerosCount) {
			arr[i] = 0
		} else {
			arr[i] = 1
		}
	}
	return arr
}

function sortBinaryArrUsingPartitioning(arr) {
	const swap = (arr, i, j) => {
		const temp = arr[i]
		arr[i] = arr[j]
		arr[j] = temp
	}

	const partition = (arr) => {
		let pivot = 1
		let j = 0
		for (let i = 0; i < arr.length; i++) {
			if (arr[i] < pivot) {
				swap(arr, i, j)
				j++
			}
		}
	}
	partition(arr)
	return arr
}

const res = sortBinaryArrUsingPartitioning([0,1,0,0,1,0,0,1,1,0])
console.log(res)

/**
 * Find index of 0 to replaced with 1 to get maximum sequence of continuous 1's
 * @param A
 */
function maxLengthOfContiguousOnes(A: number[]) {
	let maxCount = 0
	let maxIndex = -1

	let prevZeroIndex = -1
	let onesCount = 0

	for (let i = 0; i < A.length; i++) {
		if (A[i] === 1) {
			onesCount++
		} else {
			onesCount = i - prevZeroIndex
			prevZeroIndex = i
		}
		if (onesCount > maxCount) {
			maxCount = onesCount
			maxIndex = prevZeroIndex
		}
	}
	return maxIndex
}

const A = [1, 0, 1, 1, 1, 1, 1, 0, 0, 1]
const result = maxLengthOfContiguousOnes(A)
console.log(result) // 7

export {
	maxLengthOfContiguousOnes
}

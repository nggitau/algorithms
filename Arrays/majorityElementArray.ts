/**
 * Given an array of integers containing duplicates, return the majority element in an array
 * if present. A majority element appears more than n/2 times where n is the size of the array.
 * @param A
 */
function majorityElement(A: number[]): number | void {
	const n = A.length
	const obj = {}
	for (let i = 0; i < n; i++) {
		const curr = A[i]
		if (obj[curr] !== undefined) {
			obj[curr] = obj[curr] + 1
			if (obj[curr] >= Math.floor(n/2)) {
				return curr
			}
		} else {
			obj[curr] = 0
		}
	}
	return
}

// Boyer Moore Majority vote algorithm
function majorityVote(A: number[]): number | void {
	let m: number
	let i = 0
	for (let j = 0; j < A.length; j++) {
		if (i === 0) {
			m = A[j]
			i = 1
		} else if (m === A[j]) {
			i++
		} else {
			i--
		}
	}
	return m
}

const A = [2, 8, 7, 2, 2, 5, 2, 3, 1, 2, 2]
const res = majorityVote(A)
console.log(res) // 2

export {}

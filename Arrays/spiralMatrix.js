function spiralMatrix(matrix) {
	if (!matrix[0]) {
		return []
	}
	let direction = 'right'
	let rowStart = 0
	let rowEnd = matrix.length
	let colStart = 0
	let colEnd = matrix[0].length
	const result = []

	while (colStart < colEnd && rowStart < rowEnd) {
		if (direction === 'right') {
			for (let i = colStart; i < colEnd; i++) {
				result.push(matrix[rowStart][i])
			}
			direction = 'down'
			rowStart++
			continue
		}
		if (direction === 'down') {
			for (let i = rowStart; i < rowEnd; i++) {
				result.push(matrix[i][colEnd-1])
			}
			direction = 'left'
			colEnd--
			continue
		}
		if (direction === 'left') {
			for (let i = colEnd-1; i >= colStart; i--) {
				result.push(matrix[rowEnd-1][i])
			}
			direction = 'up'
			rowEnd--
			continue
		}
		if (direction === 'up') {
			for (let i = rowEnd-1; i >= rowStart; i--) {
				result.push(matrix[i][colStart])
			}
			direction = 'right'
			colStart++
			continue
		}
	}
	return result
}

let m = [
	[1,2,3,4],
	[1,2,3,4]
]
console.log(spiralMatrix(m))

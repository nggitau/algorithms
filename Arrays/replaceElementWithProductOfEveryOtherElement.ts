/**
 * Given an array of integers, replace each element of the array with product of every other
 * element in the array without using division operator.
 *
 * @param A
 */
function replaceElements(A: number[]): number[] {
	const n = A.length
	const left = []
	left[0] = 1

	for (let i = 1; i < n; i++) {
		left[i] = left[i-1] * A[i-1]
	}

	const right = []
	right[n-1] = 1

	for (let j = n-2; j >= 0; j--) {
		right[j] = right[j+1] * A[j+1]
	}

	for (let i = 0; i < n; i++) {
		A[i] = left[i] * right[i]
	}
	return A
}

// linear time and constant space (Ignoring call stack space)
function replaceElementsRecursive(A: number[]): number[] {
	helper(A, 1, 0)
	return A
}

const helper = (A: number[], left: number, currIndex: number) => {
	if (currIndex === A.length) {
		return 1
	}
	const curr = A[currIndex]
	const right = helper(A, left * curr, currIndex + 1)
	A[currIndex] = left * right
	return curr * right
}

const A = [5, 3, 4, 2, 6, 8]
const res = replaceElementsRecursive(A)
console.log(res)

export {}

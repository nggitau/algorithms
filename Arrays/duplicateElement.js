function findDuplicateElement(arr) {
	const seen = {}
	for (let item of arr) {
		if (seen[item] === undefined) {
			seen[item] = item
		} else {
			return item
		}
	}
	return 'No duplicates'
}

function findDuplicateElementConstantSpace(arr) {
	arr.sort()
	for (let i = 0; i < arr.length; i++) {
		const xor = arr[i] ^ (i+1)
		if (xor) return arr[i]
	}
	return 'No duplicates'
}

const res = findDuplicateElementConstantSpace([5,4,3,3,2,1])
console.log(res)

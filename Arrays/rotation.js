/**
 * Rotate array arr
 * @param {Array} arr
 * @param {Number} n
 * @param {String} dir
 */
function rotate(arr, n, dir) {
	const len = arr.length
	const res = []

	if (dir === 'right') {
		for (let currIdx = 0; currIdx < len; currIdx++) {
			let newIdx
			if ((currIdx + n) < len) {
				newIdx = currIdx + n
			} else {
				newIdx = ((currIdx + n) % len)
			}
			res[newIdx] = arr[currIdx]
		}
	} else if (dir === 'left') {
		for (let currIdx = 0; currIdx < len; currIdx++) {
			let newIdx
			if ((currIdx - n) >= 0) {
				newIdx = currIdx - n
			} else {
				newIdx = (len - Math.abs((currIdx - n))) % len
			}
			console.log('>> ', newIdx)
			res[newIdx] = arr[currIdx]
		}
	}
	return res
}

const res = rotate([1,2,3,4,5], 501, 'left')
console.log(res)

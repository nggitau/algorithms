/**
 * Given an array of integers, move all zeros present in the array to the end. The solution should
 * maintain the relative order of items in the array.
 *
 * @param {*} A
 */
function moveZerosToEnd(A: number[]): number[] {
	let j = A.indexOf(0)
	partition(A, j)
	return A
}

const partition = (A: number[], j: number): void => {
	for (let i = j; i < A.length; i++) {
		if (A[i] > A[j]) {
			swap(A, i, j)
			j++
		}
	}
}

const swap = (A: number[], i: number, j: number): void => {
	const temp = A[i]
	A[i] = A[j]
	A[j] = temp
}

const A = [6, 0, 8, 2, 3, 0, 4, 0, 1]
const res = moveZerosToEnd(A)
console.log(res)

export {}

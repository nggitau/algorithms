/**
 * Find subArray with k sum. Naive solution
 * @param {Array} arr
 * @param {Number} k
 */
function subArrayWithKSum(arr, k) {
	let result = []
	for (let i = 0; i < arr.length; i++) {
		let sum = 0
		for (let j = i; j < arr.length; j++) {
			sum += arr[j]
			if (sum === k) {
				result.push(arr.slice(i, j+1))
			}
		}
	}
	return result
}

function subArrayWithKSumImproved(arr, k) {
	const sums = {}
	let sum = 0
	const results = []

	for (let i = 0; i < arr.length; i++) {
		sum += arr[i]
		if (sums[sum] !== undefined) {
			const list = sums[sum]
			for (let idx of list) {
				results.push(arr.slice(idx+1, i))
			}
		} else {
			sums[sum] = [i]
		}
	}
	console.log(sums)
	return results
}
console.log([3, 4, -7, 3, 1, 3, 1, -4, -2, -2 ])
// const ans = subArrayWithKSum([3, 4, -7, 3, 1, 3, 1, -4, -2, -2 ], 0)
const ans = subArrayWithKSumImproved([3, 4, -7, 3, 1, 3, 1, -4, -2, -2 ], 37)
console.log(ans)

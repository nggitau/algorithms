/**
 * Given an array of integers, find largest sub-array formed by consecutive integers.
 * The sub-array should contain all distinct values.
 * @param {Array} arr
 */
function largestSubArrWithConsecutiveInts(arr) {
	// find all subArrays
	// find the min and max of each subArray => diff
	// get the ones with diff == length of subarray -1
	// return the longest
	let subArrays = []
	for (let i = 0; i < arr.length; i++) {
		const subArr = new Set()
		for (let j = i; j < arr.length; j++) {
			subArr.add(arr[j])
		}
		subArrays.push([...subArr])
	}

	const validSubArrays = []
	for (let sub of subArrays) {
		const subCopy = sub.concat().sort()
		const len = sub.length
		if ((subCopy[len - 1] - subCopy[0]) === len - 1) {
			validSubArrays.push(sub)
		}
	}
	return validSubArrays
		.sort((a, b) => b.length - a.length)[0]
}

const res = largestSubArrWithConsecutiveInts([1, 2, 3, 4, 5, 6, 7, 8, 9, 0])
console.log(res)

// TODO: Optimize this use O(n) space. Also for duplicates

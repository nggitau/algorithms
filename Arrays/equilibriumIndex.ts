/**
 * Given an array of integers, find equilibrium index in it.
 * For an array A consisting n elements, index i is an equilibrium index if sum of elements
 * of sub-array A[i+1..n-1] i.e. (A[0] + A[1] +...+ A[i-1]) = (A[i+1] + A[i+2] +...+ A[n-1])
 * where 0 < i < n-1. Similarly, 0 is an equilibrium index if (A[1] + .. + A[n-1]) = 0.
 *
 * @param A
 */
function equilibriumIndex(A: number[]): number[] {
	const n = A.length
	const leftToRight = [0]
	const eqIndices = []

	for (let i = 1; i < A.length; i++) {
		leftToRight[i] = leftToRight[i-1] + A[i-1]
	}

	let right = 0
	for (let i = n-1; i >= 0; i--) {
		if (leftToRight[i] === right) {
			eqIndices.push(i)
		}
		right += A[i]
	}
	return eqIndices
}

function equilibriumIndexConstantSpace(A: number[]): number[] {
	const eqIndices = []
	const arrSum = A.reduce((acc, curr) => acc + curr, 0)

	let right = 0
	for (let i = A.length - 1; i >= 0; i--) {
		if (right === arrSum - (right + A[i])) {
			eqIndices.push(i)
		}
		right += A[i]
	}
	return eqIndices
}

const A = [0, -3, 5, -4, -2, 3, 1, 0]
const res = equilibriumIndexConstantSpace(A)
console.log(res)

export {}

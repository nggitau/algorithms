/**
 * Find maximum length sub-array having given sum (k)
 * @param {Array} arr
 * @param {Number} k
 */
function maxLengthSubWithKsum(arr, k) {
	// find all subarrays with sum k
	// return the longest one
	const subArrays = []
	for (let i = 0; i < arr.length; i++) {
		let sum = 0
		for (let j = i; j < arr.length; j++) {
			sum += arr[j]
			if (sum === k) subArrays.push(arr.slice(i, j+1))
		}
	}
	console.log({subArrays})
	// to get max length, we can keep a length variable
	return subArrays.sort((a, b) => b.length - a.length)[0]
}

// const res = maxLengthSubWithKsum([3, 4, -7, 3, 1, 3, 1, -4, -2, -2], 0)
// const res = maxLengthSubWithKsum([10,5,7,1,2,5,3], 15)
// console.log(res)

function maxLenSubWithKSumOptimized(A, k) {
	const subArrays = []
	const hashmap = new Map()
	hashmap.set(0, -1)
	let sum = 0

	for (let i = 0; i < A.length; i++) {
		sum += A[i]
		const gain = sum - k
		if (hashmap.has(gain)) {
			subArrays.push(A.slice(hashmap.get(gain)+1, i+1))
		}
		hashmap.set(sum, i)
	}
	return subArrays
}

const res = maxLenSubWithKSumOptimized([10,5,7,1,2,5,3], 15)
console.log(res)

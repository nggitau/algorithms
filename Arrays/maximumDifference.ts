interface DiffType {
	diff: number;
	diffNumbers: number[]
}
/**
 * Given an array of integers, find maximum difference between two elements in the array such that
 * smaller element appears before larger element.
 *
 * @param A
 */
function maxDifference(A: number[]): DiffType {
	let smallest = A[0]
	let smallestIndex = 0
	let biggerIndex = 0
	let diff = Number.MIN_VALUE
	const diffNumbers = []

	for (let i = 1; i < A.length; i++) {
		if (A[i] < smallest) {
			smallest = A[i]
			smallestIndex = i
		}
		if (A[i] - smallest > diff) {
			diff = A[i] - smallest
			biggerIndex = i
		}
	}
	diffNumbers.push(A[smallestIndex], A[biggerIndex])
	return { diff, diffNumbers }
}

const A = [2, 7, 9, 5, 1, 3, 11]
const res = maxDifference(A)
console.log(res)

export {}

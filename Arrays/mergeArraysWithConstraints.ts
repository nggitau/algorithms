/**
 * Given two sorted arrays X[] and Y[] of size m and n each where m >= n and X[] has exactly n
 * vacant cells, merge elements of Y[] in their correct position in array X[]
 * i.e. merge (X, Y) by keeping the sorted order.
 *
 * @param A1
 * @param A2
 */
function mergeArraysWithConstraints(A1: number[], A2: number[]): number[] {
	// 1. Move non-empty elements of A1 to the beginning
	partition(A1, 0)
	return merge(A1, A2)
}

// pivot has to be in the array and has to be the smallest element.
const partition = (arr: number[], pivot: number): number[] => {
	let j = arr.indexOf(pivot)
	for (let i = 0; i < arr.length; i++) {
		if (arr[i] > pivot) {
			swap(arr, arr, i, j)
			j++
		}
	}
	return arr
}

const merge = (A1: number[], A2: number[]): number[] => {
	let i = 0
	let j = 0
	const m = A1.length
	const n = A2.length
	while (i < m && j < n) {
		if (A2[j] < A1[i]) {
			swap(A1, A2, i, j)
			i++
		} else {
			j++
		}
	}

	i = A1.indexOf(0)
	j = 0
	while (i < m && j < n) {
		if (A1[i] === 0) {
			A1[i] = A2[j]
			i++
			j++
		}
	}
	return A1
}

const swap = (A1: number[], A2: number[], i: number, j: number): void => {
	const temp = A1[i]
	A1[i] = A2[j]
	A2[j] = temp
}

const X = [0, 2, 0, 3, 0, 5, 6, 0, 0]
const Y = [1, 8, 9, 10, 15 ]
const result = mergeArraysWithConstraints(X, Y)
console.log(result) // [1, 2, 3, 5, 6, 8, 9, 10, 15]

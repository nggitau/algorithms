/**
 * Given an array of integers, rearrange the array such that every second element of the array is
 * greater than its left and right elements. Assume no duplicate elements are present in the array
 *
 * @param A
 */
function rearrangeArrayNaive(A: number[]) {
	const result = []
	A.sort()
	let i = 0
	let j = A.length - 1
	let k = 0

	while (i < j) {
		result[k++] = A[i++]
		result[k++] = A[j--]
	}
	return result
}

function rearrangeArrayOptimized(A: number[]) {
	for (let i = 1; i < A.length; i += 2) {
		if (A[i] < A[i-1]) {
			swap(A, i, i-1)
		} else if (A[i+1] > A[i]) {
			swap(A, i, i+1)
		}
	}
}

const swap = (A: number[], i: number, j: number): void => {
	const temp = A[i]
	A[i] = A[j]
	A[j] = temp
}

const A = [1, 2, 3, 4, 5, 6, 7]
rearrangeArrayOptimized(A)
console.log(A) // 1, 3, 2, 5, 4, 7, 6

export {}

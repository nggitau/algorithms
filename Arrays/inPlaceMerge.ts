/**
 * Inplace merge two sorted arrays: Given two sorted arrays X[] &amp; Y[] of size m and n each,
 * merge elements of X[] with array Y[] by maintaining the sorted order i.e.
 * fill X[] with first m smallest elements and fill Y[] with remaining elements.
 * The conversion should be done in-place without using any other data structure.
 *
 * @param {Array} A1
 * @param {Array} A2
 */
function inPlaceMerge(A1: number [], A2: number []): { arr1: number[], arr2: number[] } {
	// go through A1 and A2
	// if A2[j] < A1[i], swap
	for (let i = 0; i < A1.length; i++) {
		for (let j = 0; j < A2.length; j++) {
			if (A2[j] < A1[i]) {
				const temp = A1[i]
				A1[i] = A2[j]
				A2[j] = temp
			}
		}
	}

	return {
		arr1: A1,
		arr2: A2.sort((a, b) => a - b)
	}
}

const X = [1, 4, 7, 8, 10]
const Y = [2, 3, 9 ]

const result = inPlaceMerge(X, Y)
console.log(result)

export {}

// TODO: Explain how this can be done in O(n) time and O(1) space

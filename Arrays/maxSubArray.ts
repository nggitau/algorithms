/**
 * Given an array of integers, find contiguous subarray within it which has the largest sum.
 *
 * @param A
 */
function maxSubArray(A: number[]): number {
	let maxSoFar = A[0]
	let maxEndingHere = A[0]

	// Alternatively, for negative numbers, return max element
	// const max = Math.max(...A)
	// if (max < 0) return max

	for (let i = 1; i < A.length; i++) {
		maxEndingHere = maxEndingHere + A[i]
		maxEndingHere = Math.max(maxEndingHere, A[i])
		maxSoFar = Math.max(maxSoFar, maxEndingHere)
	}
	return maxSoFar
}

const A = [-2, 1, -3, 4, -1, 2, 1, -5, 4]
// const A = [-8,-3,-6,-2,-5,-4]
const res = maxSubArray(A)
console.log(res) // [4, -1, 2, 1] 6

export {}

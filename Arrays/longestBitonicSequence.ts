/**
 * The longest bitonic subarray problem is to find a subarray of a given sequence in which the
 * subarray’s elements are first sorted in in increasing order, then in decreasing order, and the
 * subarray is as long as possible. Strictly ascending or descending subarrays are also accepted.
 *
 * @param A
 */
function longestBitonicSequence(A: number[]): number[] {
	const n = A.length
	const left = [1]
	for (let i = 1; i < n; i++) {
		left[i] = 1
		if (A[i-1] < A[i]) {
			left[i] = left[i-1] + 1
		}
	}
	const right = []
	right[n-1] = 1
	for (let j = n-2; j >= 0; j--) {
		right[j] = 1
		if (A[j+1] < A[j]) {
			right[j] = right[j+1] + 1
		}
	}

	let lbsLength = 1
	let start = 0
	let end = 0

	for (let i = 0; i < n; i++) {
		if (lbsLength < left[i] + right[i] - 1) {
			lbsLength = left[i] + right[i] - 1
			start = i - left[i] + 1
			end = i + right[i] - 1
		}
	}
	return A.slice(start, end)
}

const A = [3, 5, 8, 4, 5, 9, 10, 8, 5, 3, 4]
const res = longestBitonicSequence(A)
console.log(res)

export {}
